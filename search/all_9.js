var searchData=
[
  ['m_46',['m',['../classCTRL_1_1CTRL.html#aa69e3eeb1a73aa5d8ce34efc243b137e',1,'CTRL.CTRL.m()'],['../classDataCollectTask_1_1DataCollectTask.html#ae7b76259ffe340cfd997eab4c52b70ed',1,'DataCollectTask.DataCollectTask.m()'],['../classENCdriver__Week__2_1_1ENCdriver__Week__2.html#a3355eb5c787e79c0c058fb9d0b340203',1,'ENCdriver_Week_2.ENCdriver_Week_2.m()']]],
  ['main_2epy_47',['main.py',['../main_8py.html',1,'']]],
  ['main_5fdatacollect_5fweek_5f1_2epy_48',['main_DataCollect_Week_1.py',['../main__DataCollect__Week__1_8py.html',1,'']]],
  ['main_5fencdriver_5fweek_5f2_2epy_49',['main_ENCdriver_Week_2.py',['../main__ENCdriver__Week__2_8py.html',1,'']]],
  ['max_50',['max',['../classClosedLoop_1_1ClosedLoop.html#a2641fd7cb69165e03695a90c6fe95054',1,'ClosedLoop::ClosedLoop']]],
  ['mot_5fdelta_51',['mot_delta',['../classCTRL_1_1CTRL.html#a297047f214dfc887976babe71b980eea',1,'CTRL::CTRL']]],
  ['mot_5fnum_52',['mot_num',['../classMOTdriver_1_1MOTdriver.html#afcdd2ab312fa1f6cfa2200c0d1498311',1,'MOTdriver::MOTdriver']]],
  ['mot_5fpos_53',['mot_pos',['../classCTRL_1_1CTRL.html#a31503be047f1ac785228ac1a71ba83c7',1,'CTRL::CTRL']]],
  ['mot_5fvel_54',['mot_vel',['../classCTRL_1_1CTRL.html#a95dd5e5a5420674999525791aed4aaaa',1,'CTRL::CTRL']]],
  ['motdriver_55',['MOTdriver',['../classMOTdriver_1_1MOTdriver.html',1,'MOTdriver']]],
  ['motor_5fcmd_56',['motor_cmd',['../elevator_8py.html#ab1e25db3ef7571e49ca9e0e41e38b1d2',1,'elevator']]]
];
