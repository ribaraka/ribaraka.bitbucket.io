var UI__frontend_8py =
[
    [ "getData", "UI__frontend_8py.html#a4a9dde01b48477da63236b2dcca631a2", null ],
    [ "baudrate", "UI__frontend_8py.html#a393e814b753fbff7d472f51f6dcc8b92", null ],
    [ "collect", "UI__frontend_8py.html#ab8f6573eb43083a664d4e871fe17bcde", null ],
    [ "delta", "UI__frontend_8py.html#a03932db83972652ef617aff4543dd7d5", null ],
    [ "n", "UI__frontend_8py.html#a001312938bbb118a02a4c154dac9d17a", null ],
    [ "newline", "UI__frontend_8py.html#a6d29b59536cc724e5ce7e09913aa603b", null ],
    [ "port", "UI__frontend_8py.html#accbd4662de73c0e8b0b52e38c926a6a1", null ],
    [ "position", "UI__frontend_8py.html#a21d95e2a638f7df272c40f001cfbe07b", null ],
    [ "splitStrings", "UI__frontend_8py.html#a409751409829d48b0567996d7d0784b0", null ],
    [ "strippedString", "UI__frontend_8py.html#a2bfbd77d372cf205151b8d43dbffee7c", null ],
    [ "t_end", "UI__frontend_8py.html#a0c7b92d9cc7445225b2faa28c1a63c59", null ],
    [ "t_start", "UI__frontend_8py.html#a6ccb2e6bc2e028d06663bf40f5b239fc", null ],
    [ "timeout", "UI__frontend_8py.html#aed91181325601e79e06ea593a2ecd619", null ],
    [ "times", "UI__frontend_8py.html#a15180f0c9fff0be1589bf5f6c7e2a2f6", null ],
    [ "user_in", "UI__frontend_8py.html#a1481cb5e892ad3b1fd38585ba1bfbc7f", null ],
    [ "values", "UI__frontend_8py.html#aa312f4ffe1650540fdc23141f07db2bb", null ],
    [ "writer", "UI__frontend_8py.html#a338e885bcfdfd9823be5421b2b276f91", null ],
    [ "zero_pos", "UI__frontend_8py.html#ac39e5b8c626bb0b63db0c84bb5c40aff", null ]
];