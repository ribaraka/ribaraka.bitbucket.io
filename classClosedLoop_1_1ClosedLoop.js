var classClosedLoop_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop_1_1ClosedLoop.html#aad39f7a5e98bf3aba683d42b8100c194", null ],
    [ "getVel", "classClosedLoop_1_1ClosedLoop.html#a53e247b70bb853a4a57b9c07b98f0cd5", null ],
    [ "run", "classClosedLoop_1_1ClosedLoop.html#a91dd769148fcfa45e97aafd8333f673d", null ],
    [ "delta_rad", "classClosedLoop_1_1ClosedLoop.html#ad0587b98c2d79884068f698d7e6699cd", null ],
    [ "encoder_delta", "classClosedLoop_1_1ClosedLoop.html#af57dbc7d62b55457ec821f87654d3a6f", null ],
    [ "k_p", "classClosedLoop_1_1ClosedLoop.html#a085268f23e0f591d843beb7d11d04a16", null ],
    [ "L", "classClosedLoop_1_1ClosedLoop.html#add9a36f67edcf30547d56b1676d53b92", null ],
    [ "max", "classClosedLoop_1_1ClosedLoop.html#a2641fd7cb69165e03695a90c6fe95054", null ],
    [ "ref", "classClosedLoop_1_1ClosedLoop.html#a251bd12741155bcc3291ed26b64d473f", null ],
    [ "timestep", "classClosedLoop_1_1ClosedLoop.html#a644f85e98567f4c6650359f3c596e043", null ],
    [ "vel", "classClosedLoop_1_1ClosedLoop.html#a6092851fd55c00e35fa72926237ec3a0", null ]
];